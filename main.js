const dataTableOptions = {
    lengthMenu: [10, 15, 20, 100, 200, 500],
    columnDefs: [
        { className: "centered", targets: [0, 1, 2, 3] },
        { orderable: false, targets: [3] },
        { searchable: false, targets: [1] }
    ],
    pageLength: 5,
    destroy: true,
    language: {
        lengthMenu: "Mostrar _MENU_ registros por página",
        zeroRecords: "Ningún usuario encontrado",
        info: "Mostrando de _START_ a _END_ de un total de _TOTAL_ registros",
        infoEmpty: "Ningún usuario encontrado",
        infoFiltered: "(filtrados desde _MAX_ registros totales)",
        search: "Buscar:",
        loadingRecords: "Cargando...",
        paginate: {
            first: "Primero",
            last: "Último",
            next: "Siguiente",
            previous: "Anterior"
        }
    }
};

const initDataTable = async () => {
    dataTable = $("#datatable_users").DataTable(dataTableOptions);
};

const clearPlaceholders = () => {
    document.getElementById('userName').value = '';
    document.getElementById('userEmail').value = '';
    document.getElementById('userPhone').value = '';
};

const addUser = () => {
    const userNameInput = document.getElementById('userName');
    const userEmailInput = document.getElementById('userEmail');
    const userPhoneInput = document.getElementById('userPhone');

    const userName = userNameInput.value.trim();
    const userEmail = userEmailInput.value.trim();

    if (!userName || !userEmail) {
        alert("Por favor, ingrese tanto el nombre como el correo electrónico del usuario.");
        return;
    }

    const existingUser = dataTable.rows().data().toArray().find(row => row[0] === userName);
    if (existingUser) {
        alert("El nombre de usuario ya está en uso.");
        return; 
    }
    
    const userRow = `
        <tr>
            <td>${userName}</td>
            <td>${userEmail}</td>
            <td>${userPhoneInput.value}</td>
            <td>
                <button class="btn btn-sm btn-danger"><i class="fa-solid fa-trash-can"></i></button>
            </td>
        </tr>`;
    
    dataTable.row.add($(userRow)).draw();
    clearPlaceholders();
};

document.getElementById('addUserBtn').addEventListener('click', addUser);

$("#datatable_users").on("click", ".btn-danger", function() {
    const userName = $(this).closest("tr").find("td:nth-child(2)").text();
    deleteUser(userName);
});

const deleteUser = (userName) => {
    if (confirm("¿Desea eliminar el contacto?")) {
        dataTable.rows().every(function() {
            const rowData = this.data();
            if (rowData[1] === userName) {
                this.remove().draw();
                return false; 
            }
            return true;
        });
    }
}

window.addEventListener("load", async () => {
    await initDataTable();
});
